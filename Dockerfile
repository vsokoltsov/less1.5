FROM debian:9

RUN apt-get update -y && \
    apt-get install -y \
    apt-transport-https \
    ca-certificates \
    curl \
    gnupg-agent \
    software-properties-common

RUN add-apt-repository \
    "deb [arch=amd64] https://download.docker.com/linux/debian \
    $(lsb_release -cs) \
    stable"

RUN apt-get update -y && apt-get install -y --allow-unauthenticated docker-ce docker-ce-cli containerd.io

RUN usermod -aG docker root && service docker start